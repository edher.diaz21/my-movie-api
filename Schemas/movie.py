import datetime
from pydantic import BaseModel, Field
from typing import Optional



class Movie(BaseModel):
    id: Optional[int] = None 
    title: str = Field(min_length=5, max_length=15)
    overview: str = Field(min_length=15, max_length=500)
    year: int =Field(le=datetime.date.today().year)
    rating: float = Field(ge=0, le=10)
    category: str = Field(min_length=1, max_length=7)
    
    class Config:
        schema_extra = {
            "example": {
                "title": "The Godfather",
                "overview": "The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.",
                "year": 1972,
                "rating": 9.2,
                "category": "Crime"
            }
        }
        exclude = ['id']
    