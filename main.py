from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from config.database import  engine, Base
from middlewares.error_handler import ErrorHandler
from routers.films import films_router
from routers.auth import auth_router


#creación de una instancia de nuestra API
app = FastAPI()
app.title = "Mi aplicación con  FastAPI"
app.version = "0.0.1"
app.add_middleware(ErrorHandler)
app.include_router(films_router)
app.include_router(auth_router)

Base.metadata.create_all(bind = engine)


#creacion del endpoin
#los tags nos permite agrupar las rutas de la aplicacion
@app.get("/", tags=['home']) #Se agrega el home para agrupar determinadas rutas
def read_root():
    return HTMLResponse('<h1 style=color:red> hola mundo </h1>') #utilizando html


