from fastapi import APIRouter
from fastapi import  HTTPException, status, Path, Query, Depends
from fastapi.responses import  JSONResponse
from middlewares.jwt_bearer import JWTBearer
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from services.movie import MovieService
from Schemas.movie import Movie


films_router = APIRouter()



#creaciòn de la ruta peliculas, y la etiqueta peliculas
@films_router.get('/films', 
         tags=['films'], 
         status_code=200,
         summary="All films",
         dependencies=[Depends(JWTBearer())])
def get_films(): #devuelve el listado de las peliculas
    """
    Obtener todas las peliculas
    """
    
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(content= jsonable_encoder(result))


@films_router.get(path="/films/read/{id}", 
        tags=["films"], 
        status_code=status.HTTP_200_OK,
        summary="Movie by id")
def get_movie_by_id(id: int = Path(ge=1, le=2000)):
    """
    Obtener película por id por parámetro de ruta
    """
    db = Session()
    result = MovieService(db).get_movie_by_id(id)
    
    if not result:
        raise HTTPException(status_code=404, detail="Id movie not found")
    
    
    """ Antes de SQLITE"""
    # movie_by_id = [movie for movie in films if movie['id'] == id]
    # if not movie_by_id:
    #     raise HTTPException(status_code=404, detail="Id movie not found")
    
    
    return JSONResponse(status_code=200, content=jsonable_encoder(result)) 


@films_router.get('/films/read/', 
        tags=['films'],
        status_code=status.HTTP_200_OK,
        summary="Movie by category our year")
def get_movie_by_category_our_year(category: str=Query(min_length=5, max_length=15, default=None), year: int = Query(default=None)):
    """
    Obtener película/as por una categoría o año por Query Parameters
    """
    
    
    db = Session()
   
    if category is None and year is None:
        
        raise HTTPException(status_code=404, detail="Escriba alguna opción")
        
    
    elif year is None:
        result = MovieService(db).get_movie_by_category(category)
        if not result:
            raise HTTPException(status_code=404, detail="Category of movie not found")
        return JSONResponse(status_code=200, content=jsonable_encoder(result))
    
    elif category is None:
        # db = Session()
        # result = db.query(MovieModel).filter(MovieModel.year == year).all()
        result = MovieService(db).get_movie_by_year(year)
        if not result:
            raise HTTPException(status_code=404, detail="Year of movie not found")
        return JSONResponse(status_code=200, content=jsonable_encoder(result))
    
    else:
        # db = Session()       
        result = MovieService(db).get_movie_by_category_year(category, year)
        if not result:
            raise HTTPException(status_code=404, detail="Category or year of movie not found")
        return JSONResponse(status_code=200, content=jsonable_encoder(result))



@films_router.post('/films/create/', 
          tags=['films'], 
          status_code= 201,
          summary="Add Movie to films", response_model=dict)
async def create_movie(movie: Movie) -> dict:
    """
    Agregar una película por parámetros en el body, ID se coloca de manera automatica segun orden que sigue en nuestros "films"
    """
    # if films:
    #     id = films[-1]['id'] + 1
    # else:
    #     id = 1
        
    # Crear la conexión   
    db = Session()
    
    MovieService(db).create_movie(movie) 
    return JSONResponse(content={"message": "Se a creado la película " + movie.title})
             


@films_router.put(
    "/update/{id}",
    tags=['films'],
    status_code=status.HTTP_200_OK,
    summary="Update movie")
async def update_movie(id: int, movie: Movie):
    """
    Actualizar una película por parámetros en el body buscando por el parámetro de ID
    """
    db = Session()
    result = MovieService(db).get_movie_by_id(id)
    # movie_by_id = [movie for movie in films if movie['id'] == id]
    if not result:
        raise HTTPException(status_code=404, detail="Movie not found! t(-_-t)")
    else:

        # movie_dict = movie.dict()
        # movie_dict['id'] = id
            
        # # films[id - 1] = movie_dict
        # result.update(movie_dict, synchronize_session = False)
        # db.commit()
        MovieService(db).update_movie(id, movie)

        return JSONResponse(status_code=200, content={"message": "Se ha modificado la pelicula " + movie.title})


@films_router.delete(
    "/delete/{id}",
    tags=['films'],
    status_code=status.HTTP_200_OK,
    summary="Delete movie")
async def delete_movie(id: int):
    """
    Eliminar una película por el parámetro de ID
    """
    db = Session()
    result = MovieService(db).get_movie_by_id(id)
    
    if not result:
        raise HTTPException(status_code=404, detail="Movie not found! t(-_-t)")
    else:
        MovieService(db).delete_movie(id)
       

    return JSONResponse(content={"message": "Se ha eliminado la pelicula "  + result.title})
