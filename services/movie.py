from models.movie import Movie as MovieModel
from Schemas.movie import Movie
class MovieService():
    def __init__(self, db) -> None:
        self.db = db
    
    def get_movies(self):
        result = self.db.query(MovieModel).all()
        return result
    
    def get_movie_by_id(self, id):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        return result
                                                  
    def get_movie_by_category(self, category):
        result =  self.db.query(MovieModel).filter(MovieModel.category == category).all()
        return result
        
      
    def get_movie_by_year(self, year):
        result =  self.db.query(MovieModel).filter(MovieModel.year == year).all()
        return result
        
      
    def get_movie_by_category_year(self, category, year):
        result =  self.db.query(MovieModel).filter(MovieModel.category == category).filter(MovieModel.year == year).all()
        return result
        
    def create_movie(self, movie: Movie):
        new_movie = MovieModel(**movie.dict())
        self.db.add(new_movie)
        self.db.commit()
        return
    
    def update_movie(self, id: int, data: Movie):
        movie = self.db.query(MovieModel).filter(MovieModel.id == id)
        
        movie_dict = data.dict()
        movie_dict['id'] = id
            
        # films[id - 1] = movie_dict
        movie.update(movie_dict, synchronize_session = False)
        self.db.commit()
        return
    
    def delete_movie(self, id: int):
        movie = self.db.query(MovieModel).filter(MovieModel.id == id).first()
           
        self. db.delete(movie)
        self.db.commit()
        return
        
        